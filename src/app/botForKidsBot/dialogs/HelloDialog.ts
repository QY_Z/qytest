import { Dialog, DialogContext, DialogTurnResult } from 'botbuilder-dialogs';

export default class HelloDialog extends Dialog {
    constructor(dialogId: string) {
        super(dialogId);
    }

    public async beginDialog(context: DialogContext, options?: any): Promise<DialogTurnResult> {
        context.context.sendActivity(`Oh, hello to you as well!`);
        return await context.endDialog();
    }
}