import { Dialog, DialogContext, DialogTurnResult, DialogTurnStatus } from 'botbuilder-dialogs';
import { StatePropertyAccessor, CardFactory, TurnContext, MemoryStorage, ConversationState, ActivityTypes, ResourceResponse, MessageFactory,} from "botbuilder";

export default class GameDialog extends Dialog {
    constructor(dialogId: string) {
        super(dialogId);
    }

    public async beginDialog(context: DialogContext, dialogId: string, choice?: any): Promise<DialogTurnResult> {
        // context.context.sendActivity(`Oh, hello to you as well!`);
        // return await context.endDialog();
        const card = CardFactory.heroCard(
            'What is the right answer?',
            ['https://example.com/whiteShirt.jpg'],
            ["A","B","C","D"]
        );
        const message = MessageFactory.attachment(card);
        await context.context.sendActivity(message);
        await prompt("game");
        var result = await context.continueDialog();
        if(result.status==DialogTurnStatus.empty){
            await context.context.sendActivity("empty");
        }else if(result.status==DialogTurnStatus.complete){
            await context.context.sendActivity("complete");
        }else if(result.status==DialogTurnStatus.cancelled){
            await context.context.sendActivity("cancelled");
        }else if(result.status==DialogTurnStatus.waiting){
            await context.context.sendActivity("waiting");
        }
        var answer=result.result;
        if (answer=="C"){
            await context.context.sendActivity("You got it!");
        }else{
            await context.context.sendActivity(result.result+"Wrong, the right answer is C");
        }
        return await context.endDialog();
    }
}