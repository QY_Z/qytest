// Components will be added here
export const nonce = {}; // Do not remove!
// Automatically added for the defaultTab tab
export * from "./defaultTab/DefaultTab";
// Automatically added for the botForKidsBot bot
export * from "./botForKidsBot/BotForKids";
