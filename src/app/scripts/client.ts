// Automatically added for the defaultTab tab
export * from "./defaultTab/DefaultTab";
export * from "./defaultTab/DefaultTabConfig";
export * from "./defaultTab/DefaultTabRemove";
// Automatically added for the botStatic bot tab
export * from "./botForKidsBot/BotStaticTab";
